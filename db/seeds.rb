# User.create!(name: "Mykola Kovalchuk",
#              email: "mikola_kovalchuk@mail.ru",
#              password: "12345678",
#              password_confirmation: "12345678"
# )
user = User.new
user.name = "Mykola Kovalchuk"
user.email = "mikola_kovalchuk@mail.ru"
user.password = "12345678"
user.password_confirmation = "12345678"
user.add_role "admin"
user.save

99.times do |n|
  # name = Faker::Name.name
  # email = "example-#{n+1}@example.com"
  # password = "password"
  # User.create!( name: name,
  #               email: email,
  #               password: password,
  #               password_confirmation: password)
  user = User.new
  user.name = Faker::Name.name
  user.email = "example-#{n+1}@example.com"
  user.password = "password"
  user.password_confirmation = "password"
  user.add_role "member"
  user.save
end


users = User.order(:created_at).take(6)
50.times do
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.tasks.create!(content: content)}
end
