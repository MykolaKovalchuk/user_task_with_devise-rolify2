class TasksController < ApplicationController
  before_action :authenticate_user!, only: [:create, :destroy, :edit]
  before_action :correct_user, only: [:destroy]

  def create
    @task = current_user.tasks.build(task_params)
    if @task.save
      flash[:success] = "Task created!"
      # redirect_to root_url
      redirect_to current_user
    else
      # render 'static_pages/home'
      flash[:danger] = "Task can't be blank or longer than 140 characters"
      redirect_to current_user
    end
  end

  def destroy
    @task.destroy
    flash[:success] = "Task deleted"
    redirect_to current_user
  end

  def edit
    @tasks = current_user.tasks.paginate(page: params[:page], per_page: 5)
    @task = @tasks.find_by(id: params[:id])
    @user = current_user
  end

  def update
    @user = current_user
    @task = current_user.tasks.find_by(id: params[:id])
    if @task.update_attributes(task_params) && !@task.nil?
      flash[:success] = "Task updated"
      redirect_to @user
    else
      flash[:danger] = "Task can't be blank or longer than 140 characters"
      redirect_to @user
    end
  end

  def create_share
    @email = ""
    @task = current_user.tasks.find_by(id: params[:id])
  end

  def share
    @user = User.find_by(email: params[:email])
    if @user
      @task = @user.tasks.build(content: params[:content], user: current_user)
      if @task.save
        flash.now[:success] = "Task created!"
        # redirect_to root_url
        redirect_to current_user
      else
        # render 'static_pages/home'
        flash.now[:danger] = "Task can't be blank or longer than 140 characters"
        redirect_to current_user
      end
    else
      flash[:danger] = "User with current email not found!"
      redirect_to current_user
    end
  end

  private

  def task_params
    params.require(:task).permit(:content)
  end

  def correct_user
    @task = current_user.tasks.find_by(id: params[:id])
    redirect_to current_user if @task.nil?
  end
end